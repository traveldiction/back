package com.esgi.traveldiction.service.geo;

import com.esgi.traveldiction.model.geo.City;

public interface CityService {

    City getCity(String name, String countryName);

    City save(City city);

}
