package com.esgi.traveldiction.service.geo;

import com.esgi.traveldiction.model.geo.City;
import com.esgi.traveldiction.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityServiceImpl implements CityService {

    CityRepository cityRepository;

    @Autowired
    public CityServiceImpl(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }


    @Override
    public City getCity(String name, String countryName) {
        List<City> cities =  cityRepository.findByNameAndCountry_Name(name, countryName);
        if (cities.stream().findFirst().isPresent())
            return cities.get(0);
        return null;
    }

    @Override
    public City save(City city) {
        return cityRepository.save(city);
    }

}
