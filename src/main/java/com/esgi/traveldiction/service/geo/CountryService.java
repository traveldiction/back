package com.esgi.traveldiction.service.geo;

import com.esgi.traveldiction.model.geo.Country;

import java.util.List;

public interface CountryService {

    public List<Country> getCountry(String name);

    public Country save(Country country);

}
