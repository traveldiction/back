package com.esgi.traveldiction.service.geo;

import com.esgi.traveldiction.model.geo.Country;
import com.esgi.traveldiction.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {

    private CountryRepository countryRepository;

    @Autowired
    public CountryServiceImpl(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public List<Country> getCountry(String name) {
        return countryRepository.findByName(name);
    }

    @Override
    public Country save(Country country) {
        return countryRepository.save(country);
    }
}
