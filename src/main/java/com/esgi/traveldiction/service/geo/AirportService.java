package com.esgi.traveldiction.service.geo;

import com.esgi.traveldiction.model.flight.Airport;

import java.util.List;

public interface AirportService {

    List<Airport> getAirports(String name);

    Airport save(Airport airport);

}
