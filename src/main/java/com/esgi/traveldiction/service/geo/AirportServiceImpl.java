package com.esgi.traveldiction.service.geo;

import com.esgi.traveldiction.model.flight.Airport;
import com.esgi.traveldiction.repository.AirportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AirportServiceImpl implements AirportService {

    AirportRepository airportRepository;

    @Autowired
    public AirportServiceImpl(AirportRepository airportRepository) {
        this.airportRepository = airportRepository;
    }

    @Override
    public List<Airport> getAirports(String name) {
        return airportRepository.findByName(name);
    }

    @Override
    public Airport save(Airport airport) {
        return airportRepository.save(airport);
    }
}
