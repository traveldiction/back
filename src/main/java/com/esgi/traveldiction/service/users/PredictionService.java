package com.esgi.traveldiction.service.users;

import com.esgi.traveldiction.model.user.Prediction;
import com.esgi.traveldiction.model.user.User;

import java.io.IOException;

public interface PredictionService {

    Prediction predictTrip(User user) throws IOException;

}
