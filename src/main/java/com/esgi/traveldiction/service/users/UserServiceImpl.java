package com.esgi.traveldiction.service.users;

import com.esgi.traveldiction.model.user.User;
import com.esgi.traveldiction.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User getUser(User user) {
        return userRepository.getBySocialUser(user.getSocialUser());
    }

    @Override
    public User writeUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    @Override
    public User deleteUser(User user) {
        return userRepository.deleteUserBySocialUser(user.getSocialUser());
    }
}
