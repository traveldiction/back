package com.esgi.traveldiction.service.users;

import com.esgi.traveldiction.model.user.User;

import java.util.List;

public interface UserService {

    public User getUser(User user);

    public User writeUser(User user);

    public List<User> getUsers();

    public User deleteUser(User user);

}
