package com.esgi.traveldiction.service.users;

import com.esgi.traveldiction.model.user.Prediction;
import com.esgi.traveldiction.model.user.User;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@Service
public class PredictionServiceImpl implements PredictionService {

    String URL = "http://127.0.0.1:8081/predict";


    @Override
    public Prediction predictTrip(User user) throws IOException {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(URL);
        httpPost.addHeader("content-type", "application/json");

        StringEntity json = new StringEntity("{" +
                "\t\"Pays\":" + "\"" + user.getCountry() + "\"" + ",\n" +
                "\t\"Age\":"  + "\"" + user.getAge() + "\"" + ",\n"+
                "\t\"Race\":"  + "\"" + "None" + "\"" + ",\n"+
                "\t\"Gender\":" + "\"" + user.getGender() + "\"" + ",\n" +
                "\t\"Status\":"  + "\"" + user.getStatus() + "\"" + ",\n" +
                "\t\"Religion\":" + "\"" + user.getReligion() + "\"" + ",\n" +
                "\t\"native_language\":" + "\"" + user.getLanguage() + "\"" + ",\n" +
                "\t\"i1\":" + "\"" + user.getInterests().get(0) + "\"" + ",\n" +
                "\t\"i2\":" + "\"" + user.getInterests().get(1) + "\"" + ",\n" +
                "\t\"i3\":" + "\"" + user.getInterests().get(2) + "\"" + "\n"+
                "}");

        httpPost.setEntity(json);

        CloseableHttpResponse response = client.execute(httpPost);
        String json_back = EntityUtils.toString(response.getEntity());

        JsonObject jsonObject = new JsonParser().parse(json_back).getAsJsonObject();

        client.close();

        List<String> countries = new ArrayList<>();

        countries.add(jsonObject.get("pred_1").getAsString());
        countries.add(jsonObject.get("pred_2").getAsString());
        countries.add(jsonObject.get("pred_3").getAsString());
        return new Prediction(countries);
    }
}
