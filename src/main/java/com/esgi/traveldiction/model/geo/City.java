package com.esgi.traveldiction.model.geo;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Objects;

@Document(indexName = "geo", type = "city")
public class City {

    @Id
    private long geoNameId; //id du dataset
    @Field(type = FieldType.Text)
    private String name;
    @Field(type = FieldType.Text)
    private String regionName;
    @Field(type = FieldType.Object)
    private Country country;
    @Field(type = FieldType.Boolean)
    private boolean isCapital;

    public City() {
        //jackson
    }

    public City(long geoNameId, String name, String regionName, Country country, boolean isCapital) {
        this.geoNameId = geoNameId;
        this.name = name;
        this.regionName = regionName;
        this.country = country;
        this.isCapital = isCapital;
    }

    @Override
    public String toString() {
        return "City{" +
                "geoNameId=" + geoNameId +
                ", name='" + name + '\'' +
                ", region name='" + regionName + '\'' +
                ", country=" + country +
                ", capital=" + isCapital +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        City city = (City) o;
        return geoNameId == city.geoNameId &&
                Objects.equals(name, city.name) &&
                Objects.equals(regionName, city.regionName) &&
                Objects.equals(country, city.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(geoNameId, name, regionName, country, isCapital);
    }

    public long getGeoNameId() {
        return geoNameId;
    }

    public void setGeoNameId(long geoNameId) {
        this.geoNameId = geoNameId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public boolean isCapital() {
        return isCapital;
    }

    public void setCapital(boolean capital) {
        isCapital = capital;
    }
}
