package com.esgi.traveldiction.model.flight;

import com.esgi.traveldiction.model.enums.Type;
import com.esgi.traveldiction.model.geo.City;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.geo.Point;

import java.util.Objects;

@Document(indexName = "geo", type = "airport")
public class Airport {

    @Id
    private String id;
    @Field(type = FieldType.Text)
    private String name;
    @Field(type = FieldType.Object)
    private City city;
    @Field(type = FieldType.Text)
    private String iata;
    @Field(type = FieldType.Text)
    private String icao;
    @Field(type = FieldType.Object)
    private Point point;
    @Field(type = FieldType.Float)
    private float timezone;
    @Field(type = FieldType.Text)
    private Type type;

    public Airport(){

    }

    public Airport(String id, String name, City city, String iata, String icao, Point point, float timezone, Type type) {
        this.id = id;
        this.name = name;
        this.city = city;
        this.iata = iata;
        this.icao = icao;
        this.point = point;
        this.timezone = timezone;
        this.type = type;
    }

    @Override
    public String toString() {
        return "Airport{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", city=" + city +
                ", iata='" + iata + '\'' +
                ", icao='" + icao + '\'' +
                ", point=" + point +
                ", timezone=" + timezone +
                ", type=" + type +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Airport airport = (Airport) o;
        return Float.compare(airport.timezone, timezone) == 0 &&
                Objects.equals(id, airport.id) &&
                Objects.equals(name, airport.name) &&
                Objects.equals(city, airport.city) &&
                Objects.equals(iata, airport.iata) &&
                Objects.equals(icao, airport.icao) &&
                Objects.equals(point, airport.point) &&
                type == airport.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, city, iata, icao, point, timezone, type);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getIata() {
        return iata;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }

    public String getIcao() {
        return icao;
    }

    public void setIcao(String icao) {
        this.icao = icao;
    }

    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }

    public float getTimezone() {
        return timezone;
    }

    public void setTimezone(float timezone) {
        this.timezone = timezone;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
