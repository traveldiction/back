package com.esgi.traveldiction.model.enums;

public enum Type {

    AIRPORT,
    STATION,
    PORT,
    UNKNOWN

}
