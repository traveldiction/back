package com.esgi.traveldiction.model.enums;

public enum Gender {

    MALE,
    FEMALE

}
