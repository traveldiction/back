package com.esgi.traveldiction.model.user;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;

import java.util.Objects;

@Document(indexName = "user_features", type = "contains_likes")
public class ContainsLikes {

    @Id
    String id;
    @Field
    String name;
    @Field
    String created_time;

    public ContainsLikes(String name, String id, String created_time) {
        this.name = name;
        this.id = id;
        this.created_time = created_time;
    }

    public ContainsLikes() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContainsLikes that = (ContainsLikes) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(id, that.id) &&
                Objects.equals(created_time, that.created_time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, id, created_time);
    }

    @Override
    public String toString() {
        return "ContainsLikes{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", created_time='" + created_time + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreated_time() {
        return created_time;
    }

    public void setCreated_time(String created_time) {
        this.created_time = created_time;
    }
}
