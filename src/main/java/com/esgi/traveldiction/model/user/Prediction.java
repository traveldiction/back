package com.esgi.traveldiction.model.user;

import java.util.List;
import java.util.Objects;

public class Prediction {

    List<String> country;

    public Prediction() {}

    public Prediction(List<String> country) {
        this.country = country;
    }

    public List<String> getCountry() {
        return country;
    }

    public void setCountry(List<String> country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "Prediction{" +
                "country=" + country +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Prediction that = (Prediction) o;
        return Objects.equals(country, that.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(country);
    }
}
