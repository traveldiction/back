package com.esgi.traveldiction.model.user;

import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;

import java.util.Objects;

@Document(indexName = "user_features", type = "age_range")
public class AgeRange {

    @Field
    int min;
    @Field
    int max;

    public AgeRange() {
    }
    public AgeRange(int min, int max) {
        this.min = min;
        this.max = max;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AgeRange ageRange = (AgeRange) o;
        return min == ageRange.min &&
                max == ageRange.max;
    }

    @Override
    public int hashCode() {
        return Objects.hash(min, max);
    }

    @Override
    public String toString() {
        return "AgeRange{" +
                "min=" + min +
                ", max=" + max +
                '}';
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }
}
