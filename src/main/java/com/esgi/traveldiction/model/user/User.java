package com.esgi.traveldiction.model.user;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.List;
import java.util.Objects;

@Document(indexName = "user_features", type = "user")
public class User {

    @Id
    private String id;
    @Field(type = FieldType.Object)
    private SocialUser socialUser;
    @Field(type = FieldType.Text)
    private List<String> interests;
    @Field(type = FieldType.Text)
    private String religion;
    @Field(type = FieldType.Text)
    private String status;
    @Field(type = FieldType.Text)
    private String language;
    @Field(type = FieldType.Text)
    private String gender;
    @Field(type = FieldType.Text)
    private String country;
    @Field(type = FieldType.Long)
    private int age;

    public User() {}

    public User(String id, SocialUser socialUser, List<String> interests, String religion, String status, String language,
                String sexe, String pays, int age) {
        this.id = id;
        this.socialUser = socialUser;
        this.interests = interests;
        this.religion = religion;
        this.status = status;
        this.language = language;
        this.gender = sexe;
        this.country = pays;
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return age == user.age &&
                Objects.equals(id, user.id) &&
                Objects.equals(socialUser, user.socialUser) &&
                Objects.equals(interests, user.interests) &&
                Objects.equals(religion, user.religion) &&
                Objects.equals(status, user.status) &&
                Objects.equals(language, user.language) &&
                Objects.equals(gender, user.gender) &&
                Objects.equals(country, user.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, socialUser, interests, religion, status, language, gender, country, age);
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", socialUser=" + socialUser +
                ", interests=" + interests +
                ", religion='" + religion + '\'' +
                ", status='" + status + '\'' +
                ", language='" + language + '\'' +
                ", sexe='" + gender + '\'' +
                ", pays='" + country + '\'' +
                ", age=" + age +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SocialUser getSocialUser() {
        return socialUser;
    }

    public void setSocialUser(SocialUser socialUser) {
        this.socialUser = socialUser;
    }

    public List<String> getInterests() {
        return interests;
    }

    public void setInterests(List<String> interests) {
        this.interests = interests;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
