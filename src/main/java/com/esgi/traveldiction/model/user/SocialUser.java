package com.esgi.traveldiction.model.user;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Objects;

@Document(indexName = "user_features", type = "SocialUser_")
public class SocialUser {

    @Id
    String id;
    @Field(type = FieldType.Text)
    String email;
    @Field(type = FieldType.Text)
    String provider;
    @Field(type = FieldType.Text)
    String name;
    @Field(type = FieldType.Text)
    String photoUrl;
    @Field(type = FieldType.Text)
    String firstName;
    @Field(type = FieldType.Text)
    String lastName;
    @Field(type = FieldType.Text)
    String authToken;
    @Field(type = FieldType.Text)
    String idToken;
    @Field(type = FieldType.Text)
    String authorizationCode;

    public SocialUser() {}

    public SocialUser(String id, String email, String provider, String name, String photoUrl, String firstName, String lastName, String authToken, String idToken, String authorizationCode) {
        this.id = id;
        this.email = email;
        this.provider = provider;
        this.name = name;
        this.photoUrl = photoUrl;
        this.firstName = firstName;
        this.lastName = lastName;
        this.authToken = authToken;
        this.idToken = idToken;
        this.authorizationCode = authorizationCode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getIdToken() {
        return idToken;
    }

    public void setIdToken(String idToken) {
        this.idToken = idToken;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SocialUser that = (SocialUser) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(email, that.email) &&
                Objects.equals(provider, that.provider) &&
                Objects.equals(name, that.name) &&
                Objects.equals(photoUrl, that.photoUrl) &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(authToken, that.authToken) &&
                Objects.equals(idToken, that.idToken) &&
                Objects.equals(authorizationCode, that.authorizationCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email, provider, name, photoUrl, firstName, lastName, authToken, idToken, authorizationCode);
    }

    @Override
    public String toString() {
        return "SocialUser{" +
                "id='" + id + '\'' +
                ", email='" + email + '\'' +
                ", provider='" + provider + '\'' +
                ", name='" + name + '\'' +
                ", photoUrl='" + photoUrl + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", authToken='" + authToken + '\'' +
                ", idToken='" + idToken + '\'' +
                ", authorizationCode='" + authorizationCode + '\'' +
                '}';
    }
}


