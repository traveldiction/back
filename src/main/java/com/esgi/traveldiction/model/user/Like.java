package com.esgi.traveldiction.model.user;

import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Objects;

@Document(indexName = "user_features", type = "user")
public class Like {

    @Field(type = FieldType.Object)
    ContainsLikes data;

    public Like() {
    }

    public Like(ContainsLikes data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Like like = (Like) o;
        return Objects.equals(data, like.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data);
    }

    @Override
    public String toString() {
        return "Like{" +
                "data=" + data +
                '}';
    }

    public ContainsLikes getData() {
        return data;
    }

    public void setData(ContainsLikes data) {
        this.data = data;
    }
}
