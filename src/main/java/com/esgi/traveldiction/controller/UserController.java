package com.esgi.traveldiction.controller;


import com.esgi.traveldiction.model.user.Prediction;
import com.esgi.traveldiction.model.user.User;
import com.esgi.traveldiction.service.users.PredictionService;
import com.esgi.traveldiction.service.users.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;
    @Autowired
    private PredictionService predictionService;

    @PostMapping("/registration")
    public User registerUser(@RequestBody User user) {
        LOG.info("User received< <{}>", user);
        return userService.writeUser(user);
    }

    @GetMapping("/hello")
    public void hello() {
        LOG.info("hello we works");
    }

    @PostMapping("/predict")
    public Prediction predictDestination(@RequestBody User user) {
        try {
            LOG.info("user got: " + user.toString());
            return predictionService.predictTrip(user);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @GetMapping("/getAll")
    public List<User> getAllusers() {
        return userService.getUsers();
    }

}
