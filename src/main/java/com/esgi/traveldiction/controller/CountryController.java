package com.esgi.traveldiction.controller;

import com.esgi.traveldiction.model.geo.Country;
import com.esgi.traveldiction.service.geo.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/country")
public class CountryController {

    @Autowired
    CountryService countryService;

    @PostMapping("/get")
    public Country getCountry(@RequestBody String name) {
        return null;
    }
}
