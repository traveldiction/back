package com.esgi.traveldiction.repository;

import com.esgi.traveldiction.model.geo.City;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CityRepository extends ElasticsearchRepository<City, String> {

    List<City> findByNameAndCountry_Name(String name, String countryName);

    City save(City city);

}
