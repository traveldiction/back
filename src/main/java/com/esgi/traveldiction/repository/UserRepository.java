package com.esgi.traveldiction.repository;

import com.esgi.traveldiction.model.user.SocialUser;
import com.esgi.traveldiction.model.user.User;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends ElasticsearchRepository<User,String> {

    User getBySocialUser(SocialUser socialUser);

    List<User> findAll();

    User deleteUserBySocialUser(SocialUser socialUser);
}
