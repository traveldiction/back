package com.esgi.traveldiction.repository;

import com.esgi.traveldiction.model.flight.Airport;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AirportRepository extends ElasticsearchRepository<Airport, String> {

    List<Airport> findByName(String name);

    Airport save(Airport airport);

}
