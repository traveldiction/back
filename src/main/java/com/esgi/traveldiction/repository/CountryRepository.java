package com.esgi.traveldiction.repository;

import com.esgi.traveldiction.model.geo.Country;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CountryRepository extends ElasticsearchRepository<Country, String> {

    List<Country> findByName(String name);

    Country save(Country country);

}
