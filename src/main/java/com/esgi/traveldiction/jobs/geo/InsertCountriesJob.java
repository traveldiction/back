/*package com.esgi.traveldiction.jobs.geo;

import com.esgi.traveldiction.model.geo.Country;
import com.esgi.traveldiction.service.geo.CountryService;
import com.opencsv.CSVReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.FileReader;

@SpringBootApplication(scanBasePackages = "com.esgi.traveldiction")
public class InsertCountriesJob implements CommandLineRunner{

    private static final String file = "D:\\Workspace\\traveldiction\\src\\main\\resources\\datasets\\countries.dat";
    private static final Logger LOG = LoggerFactory.getLogger(InsertCountriesJob.class);

    @Autowired
    public CountryService service;

    public static void main(String[] args) {
        LOG.info("STARTING THE APPLICATION");
        SpringApplication.run(InsertCountriesJob.class, args);
        LOG.info("APPLICATION FINISHED");
    }

    @Override
    public void run(String... args) throws Exception {
        LOG.info("Starting insert countries job");
        try {
            saveCountry();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveCountry() throws Exception {
        try (CSVReader csvReader = new CSVReader(new FileReader(file))) {
            String[] values = null;
            while ((values = csvReader.readNext()) != null) {
                String name = values[0];
                String iso = values[1].equals("N") ? values[2] : values[1];
                Country country = new Country(iso, name);
                LOG.info("info: country: {}", country);
                //write in db;
                service.save(country);
            }
        }
    }


}*/
