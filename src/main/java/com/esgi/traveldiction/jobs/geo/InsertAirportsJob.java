/*package com.esgi.traveldiction.jobs.geo;

import com.esgi.traveldiction.model.enums.Type;
import com.esgi.traveldiction.model.flight.Airport;
import com.esgi.traveldiction.model.geo.City;
import com.esgi.traveldiction.model.geo.Country;
import com.esgi.traveldiction.service.geo.AirportService;
import com.esgi.traveldiction.service.geo.CityService;
import com.esgi.traveldiction.service.geo.CountryService;
import com.opencsv.CSVReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.geo.Point;

import java.io.FileReader;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@SpringBootApplication(scanBasePackages = "com.esgi.traveldiction")
public class InsertAirportsJob implements CommandLineRunner {

    private static final String file = "D:\\Workspace\\traveldiction\\src\\main\\resources\\datasets\\airports.dat";
    private static final Logger LOG = LoggerFactory.getLogger(InsertAirportsJob.class);
    private boolean dryrun = false;

    @Autowired
    public CountryService countryService;

    @Autowired
    public CityService cityService;

    @Autowired
    public AirportService airportService;

    public static void main(String[] args) {
        LOG.info("STARTING THE APPLICATION");
        SpringApplication.run(InsertAirportsJob.class, args);
        LOG.info("APPLICATION FINISHED");
    }

    @Override
    public void run(String... args) throws Exception {
        LOG.info("Starting insert cities job");
        try {
            saveAirports();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveAirports() throws Exception {
        try (CSVReader csvReader = new CSVReader(new FileReader(file))) {
            String[] values = null;
            csvReader.readNext();
            long geo_id = 50000000;
            while ((values = csvReader.readNext()) != null) {
                LOG.info("values: {}", Arrays.toString(values));
                String id = values[0];
                String airportName = values[1].replace("/", " ");
                String cityName = values[2].replace("/", " ");
                String countryName = values[3];
                String iata = values[4];
                String icao = values[5];
                Point geoPoint = new Point(Double.parseDouble(values[6]), Double.parseDouble(values[7]));
                float timezone = Float.parseFloat(values[8]);
                Type type = Type.valueOf(values[12].toUpperCase());

                City city = cityService.getCity(cityName, countryName);

                if(city == null) {
                    List<Country> toFindCountry = countryService.getCountry(countryName);
                    Country country;
                    if (toFindCountry.stream().findFirst().isPresent()){
                       country = toFindCountry.stream().findFirst().get();
                       city = new City();
                       city.setGeoNameId(geo_id++);
                       city.setCountry(country);
                       city.setCapital(false);
                       city.setName(cityName);
                       city.setRegionName("UNKNOWN");
                       cityService.save(city);
                    }
                }


                Airport airport = new Airport(id, airportName, city, iata, icao, geoPoint, timezone, type);

                LOG.info("info: airport: {}", airport);
                //write in db;
                if(!dryrun)
                    airportService.save(airport);
            }
        }
    }
}*/
