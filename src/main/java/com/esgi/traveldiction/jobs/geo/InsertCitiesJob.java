/*
package com.esgi.traveldiction.jobs.geo;

import com.esgi.traveldiction.model.geo.City;
import com.esgi.traveldiction.model.geo.Country;
import com.esgi.traveldiction.service.geo.CityService;
import com.esgi.traveldiction.service.geo.CountryService;
import com.opencsv.CSVReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.FileReader;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication(scanBasePackages = "com.esgi.traveldiction")
public class InsertCitiesJob implements CommandLineRunner {

    private static final String file = "D:\\Workspace\\traveldiction\\src\\main\\resources\\datasets\\world-cities_csv.csv.txt";
    private static final Logger LOG = LoggerFactory.getLogger(InsertCitiesJob.class);
    private boolean dryrun = false;

    @Autowired
    public CityService cityService;

    @Autowired
    public CountryService countryService;

    public static void main(String[] args) {
        LOG.info("STARTING THE APPLICATION");
        SpringApplication.run(InsertCitiesJob.class, args);
        LOG.info("APPLICATION FINISHED");
    }

    @Override
    public void run(String... args) throws Exception {
        LOG.info("Starting insert cities job");
        try {
            saveCities();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveCities() throws Exception {
        try (CSVReader csvReader = new CSVReader(new FileReader(file))) {
            String[] values = null;
            csvReader.readNext();
            int i = 0;
            while ((values = csvReader.readNext()) != null) {
                //LOG.info("values: {}", Arrays.toString(values));
                if (i%100==0)
                    LOG.info("{} / {}", i++, 23017);
                String cityName = values[0];
                String countryName = values[1];
                String regionName = values[2];
                long regionId = Long.parseLong(values[3]);

                List<Country> countries = countryService.getCountry(countryName);
                if(countries.size() > 1)
                    LOG.error("Countries {}",Arrays.toString(countries.toArray()));

                Country country;
                if(countries.size() > 0) {
                    country = countries.get(0);
                } else{
                    country = null;
                }

                City city = new City(regionId, cityName, regionName , country, false);

                LOG.info("info: city: {}", city);
                //write in db;
                if(!dryrun)
                    cityService.save(city);
            }
        }
    }
}*/