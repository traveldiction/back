package com.esgi.traveldiction;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TraveldictionApplication {

	public static void main(String[] args) {
		SpringApplication.run(TraveldictionApplication.class, args);
	}

}
